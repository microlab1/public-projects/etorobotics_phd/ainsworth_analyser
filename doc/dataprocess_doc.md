# Data Process documentation

This document describes the tools used to import, synchronise and preprocess the Ainsworth Strange Situation experiment data coming from the Mocapy system and from the Solomon Coder video tagging software (both in the form of .csv files).

The available tools are the EtoData class and the Episode class. The EtoData class is written in the [dataprocess.py](../src/dataprocess.py) file. The Episode class is written in the [episode.py](../src/episode.py) file.

These tools were written by András Zöllner ([zollner.andras@gmail.com](mailto:zollner.andras@gmail.com)). 

The used Python version is Python 3.8. Required packages: pandas, numpy, matplotlib, math.

The example data processing codes can be found in the [dataprocess_examples.ipynb](src/dataprocess_examples.ipynb) jupyter notebook.
To quickly get started with the data processing tools use a copy of the [dataprocess_empty.ipynb](src/dataprocess_empty.ipynb) jupyter notebook.


## Table of Contents
[[_TOC_]]

## The EtoData class
The EtoData class is responsible for importing preprocessing and storing all the data coming from the Mocapy system and the Solomon coder. 

### Data members
- `mocapy_file`: (string), stores the path of the mocapy .csv file
- `solomon_file`: (string), stores the path of the solomon coder .csv file
- `scene_dict`: (dictionary), used to replace text based solomon coder tags with numerical values
- `tail_dict`: (dictionary), used to replace text based solomon coder tags with numerical values
- `attention_dict`: (dictionary), used to replace text based solomon coder tags with numerical values
- `contact_dict`: (dictionary), used to replace text based solomon coder tags with numerical values
- `contact_seeking_dict`: (dictionary), used to replace text based solomon coder tags with numerical values
- `room_X`: (float), size of the room in metres along the X axis
- `room_Y`: (float), size of the room in metres along the Y axis
- `room_Z`: (float), size of the room in metres along the Z axis
- `origin_offset_X` (float), offset of the mocapy coordinate frame origin along the X axis, measured in metres
- `origin_offset_Y` (float), offset of the mocapy coordinate frame origin along the Y axis, measured in metres
- `mocapy`: (pandas DataFrame), the raw mocapy data read by the pandas read_csv() function
- `solomon`: (pandas DataFrame), the raw solomon data read by the pandas read_csv() function
- `mhead`: (list of strings), the list of the column heads in the raw mocapy csv file
- `shead`: (list of strings), the list of the column heads in the raw solomon csv file
- `mocapy_scene_start_markers`: (numpy array of integers): list of the scene starting points in the mocapy file
- `mocapy_scene_end_markers`: (numpy array of integers): list of the scene ending points in the mocapy file
- `solomon_scene_start_markers`: (numpy array of integers): list of the scene starting points in the solomon file
- `solomon_scene_end_markers`: (numpy array of integers): list of the scene ending points in the solomon file
- `mocapy_sync`: (pandas DataFrame), raw mocapy data synced with solomon data
- `solomon_sync`: (pandas DataFrame), raw solomon data synced with mocapy data
- `mocapy_preproc_head`: (list of strings), the column names of the preprocessed (and synchronised) mocapy data
- `solomon_preproc_head`: (list of strings), the column names of the preprocessed (and synchronised) solomon data
- `mocapy_preproc`: (numpy array of floats), the preprocessed (and synchronised) mocapy data
- `solomon_preproc`: (numpy array of floats), the preprocessed (and synchronised) solomon data
- `preproc_scene_start`: (numpy array of integers), the scene starting points of the preprocessed (and synchronised) data
- `preproc_scene_end`: (numpy array of integers), the scene ending points of the preprocessed (and synchronised) data
- `eps`: (list of 7 Episode class instances): the preprocessed data of the different episodes distributed into and stored in Episode class instances

### The preprocessed Mocapy and Solomon data
The Mocapy and Solomon preprocessed data column header names are stored in `mocapy_preproc_head` and `solomon_preproc_head`. The meaning of the columns is the following:

#### Prerprocessed mocapy columns stored in `mocapy_preproc`
NOTE: Length (`_pos_X`, `_pos_Y`, `_pos_Z`) coordinates are normalised by adding the corresponding origin offset and then dividing by the corresponding room size. This way all X,Y,Z coordinate values are between 0 and 1. Rotational coordinates (`_rot_Z`) are also normalised by adding 180 degrees and then dividing by 360 degrees. This way all rotational coordinates are between 0 and 1.
- `Episode counter`: The number of the current episode, between the episodes this is a fraction
- `DOG_pos_X`: The dog's normalised X position.
- `DOG_pos_Y`: The dog's normalised Y position.
- `DOG_pos_Z`: The dog's normalised Z position.
- `DOG_rot_Z`: The dog's noramlised rotation.
- `DOG_tracked`: Indicates whether the dog was tracked (0/1).
- `DOG_has_toy`: Indicates whether the dog had the toy (0/1).
- `OWN_pos_X`: The owner's normalised X position.
- `OWN_pos_Y`: The owner's normalised Y position.
- `OWN_pos_Z`: The owner's normalised Z position.
- `OWN_rot_Z`: The owner's normalised rotation.
- `OWN_tracked`: Indicates whether the owner was tracked (0/1).
- `OWN_is_in_room`: Indicates whether the owner was in the room (0/1).
- `OWN_has_toy`: Indicates whether the owner has the toy (0/1).
- `OHA_pos_X`: The owner's hand's normalised X position.
- `OHA_pos_Y`: The owner's hand's normalised Y position.
- `OHA_pos_Z`: The owner's hand's normalised Z position.
- `OHA_rot_Z`: The owner's hand's normalised rotation.
- `OHA_tracked`: Indicates whether the owner's hand was tracked (0/1).
- `STR_pos_X`: The stranger's normalised X position.
- `STR_pos_Y`: The stranger's normalised Y position.
- `STR_pos_Z`: The stranger's normalised Z position.
- `STR_rot_Z`: The stranger's normalised rotation.
- `STR_tracked`: Indicates whether the stranger was tracked (0/1).
- `STR_is_in_room`: Indicates whether the stranger was in the room (0/1).
- `STR_has_toy`: Indicates whether the stranger had the toy (0/1).
- `SHA_pos_X`: The stranger's hand's normalised X position.
- `SHA_pos_Y`: The stranger's hand's normalised Y position.
- `SHA_pos_Z`: The stranger's hand's normalised Z position.
- `SHA_rot_Z`: The stranger's hand's normalised rotation.
- `SHA_tracked`: Indicates whether the stranger's hand was tracked (0/1).
- `TOY_pos_X`: The toy's normalised X position.
- `TOY_pos_Y`: The toy's normalised Y position.
- `TOY_pos_Z`: The toy's normalised Z position.
- `TOY_rot_Z`: The toy's normalised rotation.
- `TOY_tracked`: Indicates whether the toy was tracked (0/1).
- `DOOR_pos_X`: The door's normalised X position.
- `DOOR_pos_Y`: The door's normalised Y position.
- `DOOR_pos_Z`: The door's normalised Z position.
- `DOOR_rot_Z`: The door's normalised rotation.
- `DOOR_tracked`: Indicates whether the door was tracked (0/1).
- `DOOR_is_open`: Indicates whether the door was open (0/1).
- `WAND_pos_X`: The wand's normalised X position.
- `WAND_pos_Y`: The wand's normalised Y position.
- `WAND_pos_Z`: The wand's normalised Z position.
- `WAND_rot_Z`: The wand's normalised rotation.
- `WAND_tracked`: Indicates whether the wand was tracked (0/1).
- `OWN_d`: The owner's distance from the dog (in metres).
- `OWN_r`: The owner's relative normalised rotation (measured from the dog's direction).
- `OHA_d`: The owner's hand's distance from the dog (in metres).
- `OHA_r`: The owner's hand's relative normalised rotation (measured from the dog's direction).
- `STR_d`: The stranger's distance from the dog (in metres).
- `STR_r`: The stranger's relative normalised rotation (measured from the dog's direction).
- `SHA_d`: The stranger's hand's distance from the dog (in metres).
- `SHA_r`: The stranger's hand's relative normalised rotation (measured from the dog's direction).
- `TOY_d`: The toy's distance from the dog (in metres).
- `TOY_r`: The toy's relative normalised rotation (measured from the dog's direction).
- `DOOR_d`: The doors's distance from the dog (in metres). 
- `DOOR_r`: The door's relative normalised rotation (measured from the dog's direction).
#### Preprocessed solomon columns stored in `solomon_preproc`
- `Attention`: The dog's focus of attention encoded according to `attention_dict`
- `Contact`: The dog's contact encoded according to `contact_dict`
- `Contact Seeking`: The dog's contact seeking behaviour encoded according to `contact_seeking_dict`
- `Tail`: The dog's tail waging behaviour encoded according to `tail_dict`

### Member functions

#### `__init__(self, mocapy_file, solomon_file)`
Constructor of the EtoData class, opens the .csv files, reads the data and performs the preprocessing operations.
- `mocapy_file`:(string), The path of the .csv file exported from the mocapy system. 
- `solomon_file`:(string), The path of the .csv file exported from solomon coder

#### `print_rdpnums(self)`
Prints the number of raw datapoints in different episodes. Mainly used for diagnostic purposes, to check whether the preprocessed solomon and mocapy data will be the same length for each scene.

#### `get_preproc_cols(self, col_names)`
Function used to get preprocessed data from the columns specified in `col_names`.
- `col_names`: (list of strings), The columns to get the data from. Can contain either mocapy or solomon column names.
- **returns:** numpy array of the data from the requested columns
#### `raw_plot(self,fignum=1, mode= "mocapy_time", col_names = [], size=[5,5], title="", xlabel="", ylabel="")`
Function for plotting raw data.
- `fignum=1`:(int), number of figure to plot onto 
- `mode`:(string) specifies the plotting mode
    - `"mocapy_time"`: this mode plots the selected mocapy data (raw mocapy column names need to be specified in `col_names` ) against the time measured by the mocapy system
    - `"path"`: this mode plots the selected participant paths in 3D space (selected participants need to be specified in `col_names`)
    - `"sync"`: this mode plots a chart to indicate how the raw mocapy and solomon data is in sync (only the first scene starting markers are matched, the rest of the markers can be compared)
- `col_names`: (list of strings) in mocapy_time mode these are the data fields to plot against time, in path mode these are the participants whose path needs to be plotted    
- `size=[5,5]`: the size of the figure
- `title=""`: (string), the title of the plot. If set to "" the default title will appear. 
- `xlabel=""`: (string), the xlabel of the plot. If set to "" the default xlabel will appear.
- `ylabel=""`: (string), the ylabel of the plot. If set to "" the default ylabel will appear.
    
#### `plot_preproc_data(self, fignum=1, mode="points", col_names = [], size=[5,5], title="", xlabel="", ylabel="")`
Function for plotting preprocessed mocapy and solomon data. 
- `fignum=1`:(int), number of figure to plot onto 
- `mode="points"`: (string), specifies the plotting mode
    - `"points"`: default option, plots the data columns specified in `col_names`, horizontal axis will be the number of datapoint
    - `normpath`: plots the normalised path in 3D of the experiment participants specified in `col_names`
- `col_names`: (list of strings), in points mode this specifies the data columns to plot, in normpath mode this specifies the participants whose normalised path has to be plotted
- `size=[5,5]`: the size of the figure
- `title=""`: (string), the title of the plot. If set to "" the default title will appear. 
- `xlabel=""`: (string), the xlabel of the plot. If set to "" the default xlabel will appear.
- `ylabel=""`: (string), the ylabel of the plot. If set to "" the default ylabel will appear.

## The Episode class
The Episode class is used to store the preprocessed mocapy and solomon data of particular episodes. The `eps` data member of the EtoData class contains instances of this class.

### Data members
- `mocapy_preproc`: (numpy array of floats), contains the episode's preprocessed mocapy data
- `solomon_preproc`: (numpy array of floats), contains the episode's preprocessed solomon data
- `mocapy_preproc_head`: (list of strings), the names of the preprocessed mocapy data columns
- `solomon_preproc_head`: (list of strings), the names of the preprocessed solomon data columns
- `Episode_name`: (string), name of the current episode

### Member functions
#### `def __init__(self,mocapy_preproc, mocapy_preproc_head, solomon_preproc, solomon_preproc_head, Episode_name)`
Constructor of the Episode class. 
- `mocapy_preproc`: (numpy array of floats), the episode's preprocessed mocapy data
- `solomon_preproc`: (numpy array of floats), the episode's preprocessed solomon data
- `mocapy_preproc_head`: (list of strings), the names of the preprocessed mocapy data columns
- `solomon_preproc_head`: (list of strings), the names of the preprocessed solomon data columns
- `Episode_name`: (string), name of the current episode

#### `get_preproc_cols(self, col_names)`
Function used to get the episode's preprocessed data from the columns specified in `col_names`.
- `col_names`: (list of strings), The columns to get the data from. Can contain either mocapy or solomon column names.
- **returns:** numpy array of the data from the requested columns

#### `plot_preproc_data(self, fignum=1, mode="points", col_names = [], size=[5,5], title="", xlabel="", ylabel="")`
Function for plotting the episode's preprocessed mocapy and solomon data. 
- `fignum=1`:(int), number of figure to plot onto 
- `mode="points"`: (string), specifies the plotting mode
    - `"points"`: default option, plots the data columns specified in `col_names`, horizontal axis will be the number of datapoint
    - `normpath`: plots the normalised path in 3D of the experiment participants specified in `col_names`
- `col_names`: (list of strings), in points mode this specifies the data columns to plot, in normpath mode this specifies the participants whose normalised path has to be plotted
- `size=[5,5]`: the size of the figure
- `title=""`: (string), the title of the plot. If set to "" the default title will appear. 
- `xlabel=""`: (string), the xlabel of the plot. If set to "" the default xlabel will appear.
- `ylabel=""`: (string), the ylabel of the plot. If set to "" the default ylabel will appear.