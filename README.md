<img src="/doc/pics/GPK_BME_MOGI.png">

# Ainsworth Data Analysis
Balázs Nagy, András Zöllner

[[_TOC_]]

## Introduction
The aim of this project is to process the Video and Mocapy data recorded during dog Ainsworth tests. This is achieved by annotating certain behaviours on the test video and synchronising it with the mocapy data. The synchronised, annotated data is then used to train neural networks. 

The long term goal is to use these networks to assess dog Ainsworth tests hence eliminating the need of a human video annotator. Another possible application of these networks would be to use them in EtoRobotics, so that robots will be able to mimic dog behaviours more realistically.

## Useful links
[Measurements (Drive)](https://drive.google.com/drive/folders/1lQeEloyUibZBLYOo_FJfhy4wZXmpqf2c?usp=sharing) - Stored separately because of large size. <br>
[Solomon Coder](https://solomon.andraspeter.com/) - Video annotator program, for behaviour analysis. Used to create training data.  

## The examined behaviours

Possibly examinable behaviour categories:
- Attention:
    - Look at OWN
    - Look at STR
    - Look at TOY
    - Look at DOOR
- Activity
    - Playing
        - Playing with OWN
        - Playing with STR
    - Passive
        - Sit
        - Lay
        - Pet
    - Explore
    - Greet
    - Tail Wag
    - Standing at DOOR
- (Physical) Contact 
    - Contact with OWN
    - Contact with STR
- (Physical) Contact seeking
    - Contact seeking with OWN
    - Contact seeking with STR
    
Currently the Attention, Tail wagging, and Physical contact are examined as they require no professional skills to identify on video.

## Using Solomon Coder

The softwer can be run only on Windows and can be installed from the [Solomon Coder](https://solomon.andraspeter.com/) link.

**1.** Install the Solomon Coder

**2.** Run the application

**3.** Load the pre-defined config file (solomon/[solomon_config_ainsworth_small.bfc](https://gitlab.com/microlab1/private-projects/etorobotics/a15_ainsworthdataanalysis/-/blob/master/solomon/solomon_config_ainsworth_small.bcf)) 

<img src="/doc/pics/Solomon_variables.png">

The config file define a set of behavioural patterns and hot keys. 

Optional: 
- New config files can be created at any time.
- Time resulotion can be adjasted in the Coding Options/Time resolution menu.

**4.** Load a desired video file. The solomon coder only accepts .MP4 format. Measurement file stored in the Google Drive
[Measurements (Drive)](https://drive.google.com/drive/folders/1lQeEloyUibZBLYOo_FJfhy4wZXmpqf2c?usp=sharing)

**5.** Use the Solomon coder to decode behavioural sequences.

**6.** Save the sequences for further use. 


## Data (pre)processing
To efficiently process the data coming from the Mocapy System and from Solomon coder an set of software tools were created, namely the Etodata and Episode classes. 

The EtoData class is written in the [dataprocess.py](src/dataprocess.py) file, the Episode class is written in the [episode.py](src/episode.py) file. 

The detailed documentation of the data processing tools can be read [here](doc/dataprocess_doc.md).

The example data processing codes can be found in the [dataprocess_examples.ipynb](src/dataprocess_examples.ipynb) jupyter notebook.

To quickly get started with the data processing tools use a copy of the [dataprocess_empty.ipynb](src/dataprocess_empty.ipynb) jupyter notebook.

## Neural networks
The neural networks are implemented with Tensorflow Keras. Tagged datapoints are treated as different instances (not as a timeseries) and multilayer dense neural networks are used to identify the different behaviours. To detect and avoid overfitting, the collected data is separated into training, validation and test data (60-20-20%). This is done by breaking the chronologically ordered data into smaller segments. The first 60 % of a segment will be training data, and the other 20-20 will be validation and test data repsectively. To ensure that each group gets data points from every episode, the number of segments should be divisible by the number of episodes. 

The training of the networks is done in the following jupyter notebooks: 
- Attention training: [attention_train.ipynb](src/attention_train.ipynb)
- Contact training: [contact_train.ipynb](src/contact_train.ipynb)
- Tail training: [tail_train.ipynb](src/tail_train.ipynb)

The result plotting of the networks is done in the following jupyter notebooks:
- Attention result plotting: [attention_plot.ipynb](src/attention_plot.ipynb)
- Contact result plotting: [contact_plot.ipynb](src/contact_plot.ipynb)
- Tail result plotting: [tail_plot.ipynb](src/tail_plot.ipynb)

## References:
Sz. Kovács, D. Vincze, M. Gácsi, Á. Miklósi, P. Korondi, ”Ethologically inspired robot behavior implementation”, in Proc. 4th Intl. Conf. on Human System Interaction (HSI 2011), Keio University, Yokohama, Japan, 2011, pp. 64–69.

D. Vincze, M. Gácsi, Sz. Kovács, M. Niitusma, P. Korondi, Á. Miklósi, "Towards the automatic observation and coding of simple behaviours in ethological experiments", in Proc. 13th IEEE/SICE Intl. Symp. on System Integration (SII2021), Iwaki, Japan, 2021

Topál József – Miklósi Ádám – Csányi Vilmos – Dóka Antal: Attachment behavior in dogs(canis familiaris): A new application of ainsworth’s (1969) strange situation test.Journalof Comparative Psychology, 112. évf. (1998), 219–229. p. ISSN 0735-7036,1939-2087

## Opportunities, and future work
- More measurements with more dogs to avoid overfitting and to have a database on which more generalised behaviours could be trained.
- Examine different types of neural networks (maybe by treating the data as a timeseries), eg. LSTM, convolutional networks, etc.
