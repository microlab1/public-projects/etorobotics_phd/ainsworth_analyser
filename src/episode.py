import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import math

#class for storing the (raw and preprocessed) data of a particular scene
class Episode:
    def __init__(self,mocapy_preproc, mocapy_preproc_head, solomon_preproc, solomon_preproc_head, Episode_name):
        self.mocapy_preproc = mocapy_preproc.copy() #preprocessed mocapy data
        self.mocapy_preproc_head = mocapy_preproc_head.copy() #preprocessed mocapy data header
        self.solomon_preproc = solomon_preproc.copy() #preprocessed solomon data
        self.solomon_preproc_head = solomon_preproc_head.copy() #preprocessed solomon data header
        self.Episode_name = Episode_name #name of the episode

    # this function returns the normalised data specified by the colnames "names"
    def get_preproc_cols(self, col_names):
        outs = []
        for name in col_names:
            if name in self.mocapy_preproc_head:
                outs.append(self.mocapy_preproc[:, self.mocapy_preproc_head.index(name)])
            elif name in self.solomon_preproc_head:
                outs.append(self.solomon_preproc[:, self.solomon_preproc_head.index(name)])
        out = outs[0].reshape((-1, 1))
        for i in range(1, len(outs)):
            out = np.append(out, outs[i].reshape((-1, 1)), axis=1)
        return out

    #this function plots the episode's preprocessed mocapy and solomon data
    def plot_preproc_data(self, fignum=1, mode="points", col_names = [], size=[5,5], title="", xlabel="", ylabel=""):
        if mode == "points": #plot preprocessed data with number of datapoints on horizontal axis
            plt.figure(num=fignum,figsize=size)
            for col in col_names:
                if (col in self.mocapy_preproc_head) | (col in self.solomon_preproc_head):
                    plt.plot(self.get_preproc_cols([col]),label=col)
            plt.title(title)
            plt.xlabel(xlabel)
            plt.ylabel(ylabel)
            plt.legend()
            plt.show()

        elif mode == "normpath": #plotting the experiment's participants' normalised path
            plt.figure(num=fignum, figsize=size)
            ax = plt.axes(projection="3d")
            for participant in col_names:
                plt.plot(self.get_preproc_cols([participant+"_pos_X"]).reshape(-1), self.get_preproc_cols([participant + "_pos_Y"]).reshape(-1),
                         self.get_preproc_cols([participant + "_pos_Z"]).reshape(-1), label=participant)
            ax.set_xlabel("X [norm]")
            ax.set_ylabel("Y [norm]")
            ax.set_zlabel("Z [norm]")
            if title == "":
                plt.title(self.Episode_name + " Particpant paths (normalised coordinates)")
            else:
                plt.title(title)
            plt.legend(loc=2)
            plt.show()


