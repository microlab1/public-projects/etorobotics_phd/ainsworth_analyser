import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import math
from episode import *


class EtoData:
    def __init__(self, mocapy_file, solomon_file):
        self.mocapy_file  = mocapy_file
        self.solomon_file = solomon_file

        # dictionaries of solomon behaviours
        self.scene_dict = {0: None, 1: "New Scene", 2: "Scene End"}
        self.tail_dict = {0: None, 1: "Tail wag"}
        self.attention_dict = {0: None, 1: "look at owner", 2: "look at stranger", 3: "look at door", 4: "look at toy"}
        self.contact_dict = {0: None, 1: "Contact with OWN", 2: "Contact with STR"}
        self.contact_seeking_dict = {0: None}

        #finding the header and config data in the mocapy file
        f = open(mocapy_file, "r")
        lines = f.readlines()
        f.close()
        hdr = lines.index("[MEASUREMENT]\n") + 1
        for i in range(hdr):
            if lines[i] == "\n":
                hdr -= 1

        #reading configuration data from the mocapy file
        cfg = lines.index("[CONFIG]\n")
        self.room_X = float(lines[cfg+1].partition(":")[2])
        self.room_Y = float(lines[cfg + 2].partition(":")[2])
        self.room_Z = 2.0
        self.origin_offset_X = float(lines[cfg + 4].partition(":")[2])
        self.origin_offset_Y = float(lines[cfg + 5].partition(":")[2])

        #reading the raw data with pandas
        self.mocapy = pd.read_csv(self.mocapy_file, header=hdr)
        self.solomon = pd.read_csv(self.solomon_file, header=0, sep=";")

        #obtaining the raw data headers
        self.mhead = self.mocapy.columns.values.tolist()
        self.shead = self.solomon.columns.values.tolist()

        # extract scene start and endpoints from mocapy
        self.mocapy_scene_start_markers = []
        for i in range(len(self.mocapy)):
            if i > 1:
                if (self.mocapy["Episode counter"][i] - math.floor(self.mocapy["Episode counter"][i]) == 0) & (
                        self.mocapy["Episode counter"][i - 1] - math.floor(self.mocapy["Episode counter"][i - 1]) != 0):
                    self.mocapy_scene_start_markers.append(i)
        self.mocapy_scene_start_markers = np.array(self.mocapy_scene_start_markers)
        self.mocapy_scene_end_markers = self.mocapy_scene_start_markers + 5999
        
        #extract scene start and endpoints from the solomon coder csv
        self.solomon_scene_start_markers = [] #list for scene startpoints
        self.solomon_scene_end_markers = [] #list for scene endpoints
        for i in range(len(self.solomon)):
            if self.solomon["Scene"][i] == "New Scene":
                self.solomon_scene_start_markers.append(i)
                self.solomon_scene_end_markers.append(i+5999) #mocapy has preicsely 6000 datapoints / scene --> 6000 solomon points are extracted / scene
            #scene end markers are not used since the number of data points in solomon and in mocapy could be different
            #elif self.solomon["Scene"][i] == "Scene End":
            #    self.solomon_scene_end_markers.append(i)
        self.solomon_scene_start_markers = np.array(self.solomon_scene_start_markers)
        self.solomon_scene_end_markers = np.array(self.solomon_scene_end_markers)

        #data preprocessing

        #get the synchronised raw data
        syncend = min(len(self.mocapy.loc[self.mocapy_scene_start_markers[0]:]),
                      len(self.solomon.loc[self.solomon_scene_start_markers[0]:]))
        self.mocapy_sync = self.mocapy.loc[self.mocapy_scene_start_markers[0]: self.mocapy_scene_start_markers[0] + (syncend - 1)].copy()
        self.solomon_sync = self.solomon.loc[self.solomon_scene_start_markers[0]: self.solomon_scene_start_markers[0] + (syncend - 1)].copy()
        self.mocapy_sync.reset_index(inplace=True, drop=True)
        self.solomon_sync.reset_index(inplace=True, drop=True)

        #preprocessing the raw mocapy data (inputs)
        self.mocapy_preproc_head = self.mocapy_sync.columns.values.tolist()
        self.mocapy_preproc = self.mocapy_sync.to_numpy()
        self.mocapy_preproc[:,1] = self.mocapy_preproc[:,1] - self.mocapy_preproc[0,1] #start counting time from the beginning of synced data

        xind = [4, 10, 17, 22, 29, 34, 39, 45]  # index of X coordinates
        for i in xind:
            #every X coordinate column is followed by a Y, Z and rotZ coordinate
            self.mocapy_preproc[:, i] = (self.mocapy_preproc[:, i] + self.origin_offset_X) / self.room_X  # normalisation of X coordinates
            self.mocapy_preproc[:, i + 1] = (self.mocapy_preproc[:,i + 1] + self.origin_offset_Y) / self.room_Y  # normalisation of Y coordinates
            self.mocapy_preproc[:, i + 2] = self.mocapy_preproc[:, i + 2] / self.room_Z  # normalisation of Z coordinates
            self.mocapy_preproc[:, i + 3] = (self.mocapy_preproc[:,
                                             i + 3] + 180.0) / 360.0  # normalisation of rot Z coordinates
        delind = [50, 3, 1, 0]  # deleting fields: Frame, Time, Episode name, Unnamed: 50
        self.mocapy_preproc = np.delete(self.mocapy_preproc, delind, axis=1)  # delete the unneeded columns
        delind.sort(reverse=True)
        for i in delind:
            self.mocapy_preproc_head.pop(i)  # deleting the unwanted column names
        self.mocapy_preproc = np.float32(self.mocapy_preproc)  # convert every value to float32

        #calculate extra inputs (mocapy relative distances and relative normalised rotations)

        #nested function to add object's relative distance (measured from DOG) to mocapy_preproc, call with object's name
        #this relative distance value is not normalised
        def add_d(obj):
            d = np.concatenate([(self.get_preproc_cols([obj+"_pos_X"]) - self.get_preproc_cols(["DOG_pos_X"])) * self.room_X,
                                (self.get_preproc_cols([obj+"_pos_Y"]) - self.get_preproc_cols(["DOG_pos_Y"])) * self.room_Y,
                                (self.get_preproc_cols([obj+"_pos_Z"]) - self.get_preproc_cols(["DOG_pos_Z"])) * self.room_Z], axis=1)
            #d = self.get_preproc_cols([obj+"_pos_X", obj+"_pos_Y", obj+"_pos_Z"]) - self.get_preproc_cols(["DOG_pos_X", "DOG_pos_Y", "DOG_pos_Z"])
            d = np.sum(np.square(d),axis=1).reshape((-1,1))
            d = np.sqrt(d)
            self.mocapy_preproc = np.append(self.mocapy_preproc, d, axis=1)
            self.mocapy_preproc_head.append(obj + "_d")

        #nested function to add object's relative rotation (measured from DOG) to mocapy_preproc, call with object's name
        #this rotation value is normalised
        def add_r(obj):
            r = self.get_preproc_cols([obj+"_rot_Z"]) - self.get_preproc_cols(["DOG_rot_Z"])
            self.mocapy_preproc = np.append(self.mocapy_preproc, r, axis=1)
            self.mocapy_preproc_head.append(obj + "_r")

        add_d("OWN") #add OWNer's distance (measured from DOG)
        add_r("OWN") #add OWNer's relative normalised rotaion (measured from DOG)
        add_d("OHA") #add Owner's HAnd's distance (measured from DOG)
        add_r("OHA") #add Owner's HAnd's relative normalised rotaion (measured from DOG)
        add_d("STR") #add STRanger's distance (measured from DOG)
        add_r("STR") #add STRanger's relative normalised rotation (measured from DOG)
        add_d("SHA") #add Stranger's HAnd's distance (measured from DOG)
        add_r("SHA") #add Stranger's HAnd's relative normalised rotation (measured from DOG)
        add_d("TOY")  #add TOY's relative distance (measured from DOG)
        add_r("TOY")  #add TOY's relative normalised rotation (measured from DOG)
        add_d("DOOR")  #add DOOR's relative distance (measured from DOG)
        add_r("DOOR")  #add DOOR's relative normalised rotation (measured from DOG)

        # preprocessing the raw solomon data (tags)
        #first the strings have to be replaced with numbers according to the behaviour dictionaries
        self.solomon_sync["Scene"] = self.solomon_sync["Scene"].replace(self.scene_dict.values(), self.scene_dict.keys())
        self.solomon_sync["Attention"] = self.solomon_sync["Attention"].replace(self.attention_dict.values(),
                                                                                self.attention_dict.keys())
        self.solomon_sync["Contact"] = self.solomon_sync["Contact"].replace(self.contact_dict.values(),
                                                                            self.contact_dict.keys())
        self.solomon_sync["Contact Seeking"] = self.solomon_sync["Contact Seeking"].replace(
            self.contact_seeking_dict.values(), self.contact_seeking_dict.keys())
        self.solomon_sync["Tail"] = self.solomon_sync["Tail"].replace(self.tail_dict.values(), self.tail_dict.keys())

        self.solomon_preproc = self.solomon_sync.to_numpy()  # pandas frame to numpy array
        self.solomon_preproc_head = self.solomon_sync.columns.values.tolist()  # pandas columns to list
        delind = [1, 0] #index of unneeded columns to be deleted
        self.solomon_preproc = np.delete(self.solomon_preproc, delind, axis=1)  # delete the unneeded columns
        delind.sort(reverse=True)
        for i in delind:
            self.solomon_preproc_head.pop(i)  # deleting the unwanted column names
        self.solomon_preproc = np.float32(self.solomon_preproc)  # convert every value to float32

        #end of data preprocessing

        # preprocess scene start and end markers
        self.preproc_scene_start = self.mocapy_scene_start_markers - self.mocapy_scene_start_markers[0]
        self.preproc_scene_end = self.mocapy_scene_end_markers - self.mocapy_scene_start_markers[0] + 1

        #distribute the data according to episodes
        self.eps = []
        for i in range(7):
            #mocapy_preproc, mocapy_preproc_head, solomon_preproc, solomon_preproc_head, Episode_name
            self.eps.append(Episode(self.mocapy_preproc[self.preproc_scene_start[i]:self.preproc_scene_end[i]],
                                    self.mocapy_preproc_head,
                                    self.solomon_preproc[self.preproc_scene_start[i]:self.preproc_scene_end[i]],
                                    self.solomon_preproc_head,
                                    self.mocapy["Episode name"][self.mocapy_scene_start_markers[i]]))


    # prints the number of raw mocapy and video data points in each episode
    def print_rdpnums(self):
        print("Number of data points:")
        print("Ep.\tMocapy\tVideo")
        for i in range(7):
            ep_mocapy = self.mocapy.loc[self.mocapy_scene_start_markers[i] : self.mocapy_scene_end_markers[i]]
            ep_solomon = self.solomon.loc[self.solomon_scene_start_markers[i] : self.solomon_scene_end_markers[i]]
            print(str(i+1) + "\t" + str(len(ep_mocapy)) + "\t" + str(len(ep_solomon)))

    # this function returns the normalised data specified by the colnames "names"
    def get_preproc_cols(self, col_names):
        outs = []
        for name in col_names:
            if name in self.mocapy_preproc_head:
                outs.append(self.mocapy_preproc[:, self.mocapy_preproc_head.index(name)])
            elif name in self.solomon_preproc_head:
                outs.append(self.solomon_preproc[:, self.solomon_preproc_head.index(name)])
        out = outs[0].reshape((-1,1))
        for i in range(1,len(outs)):
            out = np.append(out, outs[i].reshape((-1,1)), axis=1)
        return out

    #this function plots raw data
    def raw_plot(self,fignum=1, mode= "mocapy_time", col_names = [], size=[5,5], title="", xlabel="", ylabel=""):
        if mode == "mocapy_time": #plot raw mocapy data with mocapy time on horizontal axis
            plt.figure(num=fignum, figsize=size)
            for col in col_names:
                if col in self.mocapy.columns:
                    plt.plot(self.mocapy["Time"], self.mocapy[col], label=col)
            if xlabel == "":
                plt.xlabel("Time [s]")
            else:
                plt.xlabel(xlabel)
            plt.ylabel(ylabel)
            plt.legend()
            plt.title(title)
            plt.show()
        elif mode == "path": #plotting the experiment's participants' path
            plt.figure(num=fignum, figsize=size)
            ax = plt.axes(projection="3d")
            for participant in col_names:
                plt.plot(self.mocapy[participant+"_pos_X"], self.mocapy[participant+"_pos_Y"], self.mocapy[participant+"_pos_Z"], label=participant)
            ax.set_xlabel("X [m]")
            ax.set_ylabel("Y [m]")
            ax.set_zlabel("Z [m]")
            if title == "":
                plt.title("Particpant paths")
            else:
                plt.tile(title)
            plt.legend(loc=2)
            plt.show()
        elif mode == "sync": #plot data synchronisation chart
            plt.figure(num=fignum, figsize=size)
            plt.plot(self.mocapy["Time"], self.mocapy["DOOR_pos_X"], label="Door X (from mocapy)")
            plt.plot(self.mocapy["Time"], self.mocapy["DOOR_pos_Y"], label="Door Y (from mocapy)")
            plt.plot(self.mocapy["Time"], self.mocapy["DOOR_is_open"], label="Door open (from mocapy)")
            plt.plot(self.mocapy["Time"], self.mocapy["Episode counter"], label="Episode counter (from mocapy)")
            plt.scatter(self.mocapy["Time"][(self.solomon_scene_start_markers - self.solomon_scene_start_markers[0] + self.mocapy_scene_start_markers[0])],
                        self.mocapy["Episode counter"][(self.solomon_scene_start_markers - self.solomon_scene_start_markers[0] + self.mocapy_scene_start_markers[0])],
                        label="New Scene (from solomon)")
            plt.scatter(self.mocapy["Time"][(self.solomon_scene_end_markers - self.solomon_scene_start_markers[0] + self.mocapy_scene_start_markers[0])],
                        self.mocapy["Episode counter"][(self.solomon_scene_end_markers - self.solomon_scene_start_markers[0] + self.mocapy_scene_start_markers[0])],
                        label="Scene End (from solomon)")
            plt.scatter(self.mocapy["Time"][self.mocapy_scene_start_markers],
                        self.mocapy["Episode counter"][self.mocapy_scene_start_markers],
                        label="New Scene (from mocapy)")
            plt.scatter(self.mocapy["Time"][self.mocapy_scene_end_markers],
                        self.mocapy["Episode counter"][self.mocapy_scene_end_markers],
                        label="Scene End (from mocapy)")
            if title == "":
                plt.title("Sync chart")
            else:
                plt.title(title)
            if xlabel == "":
                plt.xlabel("Mocapy time [s]")
            else:
                plt.xlabel(xlabel)
            plt.ylabel(ylabel)
            plt.legend()
            plt.show()

    #this function is used to plot preprocessed data
    def plot_preproc_data(self, fignum=1, mode="points", col_names = [], size=[5,5], title="", xlabel="", ylabel=""):
        if mode == "points": #plot preproc data with the number of datapoints on horizontal axis
            plt.figure(num=fignum,figsize=size)
            for col in col_names:
                if (col in self.mocapy_preproc_head) | (col in self.solomon_preproc_head):
                    plt.plot(self.get_preproc_cols([col]),label=col)
            plt.title(title)
            plt.xlabel(xlabel)
            plt.ylabel(ylabel)
            plt.legend()
            plt.show()

        elif mode == "normpath": #plotting the experiment's participants' normalised path
            plt.figure(num=fignum, figsize=size)
            ax = plt.axes(projection="3d")
            for participant in col_names:
                plt.plot(self.get_preproc_cols([participant+"_pos_X"]).reshape(-1), self.get_preproc_cols([participant+"_pos_Y"]).reshape(-1),
                         self.get_preproc_cols([participant+"_pos_Z"]).reshape(-1), label=participant)
            ax.set_xlabel("X [norm]")
            ax.set_ylabel("Y [norm]")
            ax.set_zlabel("Z [norm]")
            if title == "":
                plt.title("Particpant paths (normalised coordinates)")
            else:
                plt.title(title)
            plt.legend(loc=2)
            plt.show()

#testing the class on its own (outside jupyter notebook)
if __name__ == "__main__":
    mocapy_file = "../log/Husky_02-processed [MISLABELED EPISODES].csv" #mocapy .csv file path
    video_file = "../log/vid_tags_time_refined.csv" #solomon .csv file path
    myData = EtoData(mocapy_file, video_file) #creating an EtoData instance
